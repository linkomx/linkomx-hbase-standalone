# Linko MX - HBase Standalone

Docker image of the HBase in Standalone mode.

## Requirements

  - Docker 20

## Contributing

### Git config

```bash
$ git config --local "user.name" "myusernameatgitlab"
```

```bash
$ git config --local "user.email" "myemail@linko.mx"
```

## Building

### Local installation

For local installation use:

```bash
$ docker run \
    -u $(id -u):$(grep -w docker /etc/group | awk -F\: '{print $3}') \
    --rm \
    -w $(pwd) \
    -v /etc/group:/etc/group:ro \
    -v /etc/passwd:/etc/passwd:ro \
    -v $(pwd):$(pwd) \
    -v ${HOME}/.m2:${HOME}/.m2 \
    -v /var/run/docker.sock:/var/run/docker.sock \
    azul/zulu-openjdk-alpine:8u282 \
    ./mvnw -Djansi.force=true -ntp -U clean package
```

Then launch the HBase in _standalone_ mode with:

```bash
$ docker run -d \
    --net=host \
    --rm \
    --name=hbase \
    linkomx-hbase-standalone:2.2.7
```

